class Card {
    constructor(title, text, userName, userEmail) {
        this.title = title;
        this.text = text;
        this.userName = userName;
        this.userEmail = userEmail;
    }

    createCardElement() {
        const cardElement = document.createElement('div');
        cardElement.style.width = '500px';
        cardElement.style.margin = 'auto';
        cardElement.style.marginBottom = '20px'; 
        cardElement.style.borderWidth = '2px'; 
        cardElement.style.borderStyle = 'solid'; 
        cardElement.style.borderColor = 'grey'; 
        cardElement.classList.add('card');

        cardElement.innerHTML = `
            <div class="card-header">
                <h2>${this.title}</h2>
            </div>
            <div class="card-body">
                <p>${this.text}</p>
                <div class="user-info">
                    <p>${this.userName}</p>
                    <p>${this.userEmail}</p>
                </div>
            </div>
        `;

        return cardElement;
    }
}

Promise.all([
    fetch('https://ajax.test-danit.com/api/json/users').then(response => response.json()),
    fetch('https://ajax.test-danit.com/api/json/posts').then(response => response.json())
])

.then(([users, posts]) => {
    console.log(users);
    console.log(posts);

    posts.forEach(post => {
        const matchingUser = users.find(user => user.id === post.userId);
        if (matchingUser) {
            const card = new Card(post.title, post.body, matchingUser.name, matchingUser.email);
            const cardElement = card.createCardElement();

            const deleteButton = document.createElement('button');
            deleteButton.textContent = 'Delete'; 
            deleteButton.addEventListener('click', () => {
                fetch(`https://ajax.test-danit.com/api/json/posts/${post.id}`, { method: 'DELETE' })
                .then(response => {
                    if (response.ok) {
                        cardElement.remove();
                    } else {
                        console.error('Error deleting post');
                    }
                })
                .catch(error => {
                    console.error('Error deleting post', error);
                });
            });

            cardElement.appendChild(deleteButton);
            document.body.appendChild(cardElement);
        }
    });
})
.catch(error => {
    console.error('Error', error);
});
